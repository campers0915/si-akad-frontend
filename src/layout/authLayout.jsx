import React from "react";
import { useLocation, Navigate, Outlet } from "react-router-dom";

export default function AuthLayout(props) {
  const location = useLocation();
  const token = localStorage.getItem("token");

  if (token) {
    return <Navigate to="/" state={{ from: location }} replace />;
  }

  return <Outlet />;
}
