import React from "react";
import { useLocation, Navigate, Outlet } from "react-router-dom";

import Sidebar from "../components/sidebar";
import Footer from "../components/footer";

export default function MainLayout() {
  const location = useLocation();
  const token = localStorage.getItem("token");

  if (!token) {
    return <Navigate to="/login" state={{ from: location }} replace />;
  }
  return (
    <div className="container-fluid overflow-hidden">
      <div className="row vh-100 overflow-auto">
        <div className="col-12 col-sm-3 col-xl-2 px-sm-2 px-0 bg-white d-flex sticky-top border-end-1 shadow">
          <Sidebar />
        </div>
        <div className="col d-flex flex-column h-sm-100">
          <main className="row overflow-auto">
            <div className="col pt-4 mx-3">
              <Outlet />
            </div>
          </main>
          <Footer />
        </div>
      </div>
    </div>
  );
}
