import React from "react";

export default function Dashboard() {
  return (
    <>
      <h3>Dashboard</h3>
      <hr />
      <h3>More content...</h3>
      <p>
        Sriracha biodiesel taxidermy organic post-ironic, Intelligentsia salvia
        mustache 90's code editing brunch. Butcher polaroid VHS art party,
        hashtag Brooklyn deep v PBR narwhal sustainable mixtape swag wolf squid
        tote bag. Tote bag cronut semiotics, raw denim deep v taxidermy
        messenger bag. Tofu YOLO Etsy, direct trade ethical Odd Future jean
        shorts paleo. Forage Shoreditch tousled aesthetic irony, street art
        organic Bushwick artisan cliche semiotics ugh synth chillwave
        meditation. Shabby chic lomo plaid vinyl chambray Vice. Vice sustainable
        cardigan, Williamsburg master cleanse hella DIY 90's blog.
      </p>

      <h3>Table</h3>
      <table class="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">First</th>
            <th scope="col">Last</th>
            <th scope="col">Handle</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">1</th>
            <td>Mark</td>
            <td>Otto</td>
            <td>@mdo</td>
            <td>
              <button className="btn btn-sm btn-secondary me-1">Update</button>
              <button className="btn btn-sm btn-danger">Delete</button>
            </td>
          </tr>
          <tr>
            <th scope="row">2</th>
            <td>Jacob</td>
            <td>Thornton</td>
            <td>@fat</td>
            <td>
              <button className="btn btn-sm btn-secondary me-1">Update</button>
              <button className="btn btn-sm btn-danger">Delete</button>
            </td>
          </tr>
          <tr>
            <th scope="row">3</th>
            <td colspan="2">Larry the Bird</td>
            <td>@twitter</td>
            <td>
              <button className="btn btn-sm btn-secondary me-1">Update</button>
              <button className="btn btn-sm btn-danger">Delete</button>
            </td>
          </tr>
        </tbody>
      </table>
    </>
  );
}
