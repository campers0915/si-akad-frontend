import React from "react";
import imgRegister from "../../assets/img/signup-image.jpg";
import { Link } from "react-router-dom";

export default function Register() {
  return (
    <div className="container d-flex justify-content-center align-items-center vh-100">
      <div className="card p-5 shadow" style={{ width: "50rem" }}>
        <div className="row">
          <div className="col-6 d-flex flex-column justify-content-center">
            <h2>Register</h2>
            <form>
              <div className="mb-3">
                <label className="form-label">Email</label>
                <input
                  type="email"
                  className="form-control"
                  placeholder="Input your email ..."
                />
              </div>
              <div className="mb-3">
                <label className="form-label">Password</label>
                <input
                  type="password"
                  className="form-control"
                  placeholder="Input your password ..."
                />
              </div>
              <div className="mb-3">
                <label className="form-label">Confirm Password</label>
                <input
                  type="password"
                  className="form-control"
                  placeholder="Input your password ..."
                />
              </div>
              <button type="submit" className="btn btn-primary">
                Submit
              </button>
            </form>
            <small className="mt-3">
              Already have an account? <Link to="/login">Login</Link>
            </small>
          </div>
          <div className="col-6 d-flex flex-column align-items-center justify-content-center">
            <figure>
              <img src={imgRegister} alt="Register" />
            </figure>
          </div>
        </div>
      </div>
    </div>
  );
}
