import React from "react";
import imgLogin from "../../assets/img/signin-image.jpg";
import { Link, useNavigate } from "react-router-dom";

export default function Login() {
  const navigate = useNavigate();
  const handleLogin = () => {
    localStorage.setItem("token", "TOKEN123");
    navigate("/");
  };
  return (
    <div className="container d-flex justify-content-center align-items-center vh-100">
      <div className="card p-5 shadow" style={{ width: "50rem" }}>
        <div className="row">
          <div className="col-6 d-flex flex-column align-items-center justify-content-center">
            <figure>
              <img src={imgLogin} alt="Login" />
            </figure>
          </div>
          <div className="col-6 d-flex flex-column justify-content-center">
            <h2>Login</h2>
            <form>
              <div className="mb-3">
                <label className="form-label">Email</label>
                <input
                  type="email"
                  className="form-control"
                  placeholder="Input your email ..."
                />
              </div>
              <div className="mb-3">
                <label className="form-label">Password</label>
                <input
                  type="password"
                  className="form-control"
                  placeholder="Input your password ..."
                />
              </div>
              <button
                type="submit"
                className="btn btn-primary"
                onClick={handleLogin}
              >
                Submit
              </button>
            </form>
            <small className="mt-3">
              Don't have an account yet? <Link to="/register">Register</Link>
            </small>
          </div>
        </div>
      </div>
    </div>
  );
}
