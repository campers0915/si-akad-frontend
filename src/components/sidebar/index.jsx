import React from "react";
import defaultImage from "../../assets/img/default.png";

import { useNavigate } from "react-router-dom";

export default function Sidebar() {
  const navigate = useNavigate();
  const handleLogout = () => {
    localStorage.clear();
    navigate("/login");
  };
  return (
    <div className="d-flex flex-sm-column flex-row flex-grow-1 align-items-center align-items-sm-start px-3 pt-2">
      <div
        onClick={() => navigate()}
        className="d-flex align-items-center pb-sm-3 mb-md-0 me-md-auto text-decoration-none fw-bold text-secondary"
        type="button"
      >
        <span className="fs-5">
          <i className="fs-5 bi-file-text"></i>
          <span className="d-none d-sm-inline"> Si Akad</span>
        </span>
      </div>
      <ul
        className="nav nav-pills flex-sm-column flex-row flex-nowrap flex-shrink-1 flex-sm-grow-0 flex-grow-1 mb-sm-auto mb-0 justify-content-center align-items-center align-items-sm-start"
        id="menu"
      >
        <li>
          <a href="#submenu1" className="nav-link px-sm-0 px-2 text-secondary">
            <i className="fs-5 bi-file-earmark-bar-graph"></i>
            <span className="ms-1 d-none d-sm-inline">Dashboard</span>
          </a>
        </li>
        <li className="dropdown">
          <a
            href="#"
            className="nav-link dropdown-toggle px-sm-0 px-1 text-secondary"
            id="dropdown"
            data-bs-toggle="dropdown"
            aria-expanded="false"
          >
            <i className="fs-5 bi-file-earmark-person"></i>
            <span className="ms-1 d-none d-sm-inline">Data User</span>
          </a>

          <ul
            className="dropdown-menu dropdown-menu-dark text-small shadow"
            aria-labelledby="dropdown"
          >
            <li>
              <a className="dropdown-item" href="#">
                Siswa
              </a>
            </li>
            <li>
              <a className="dropdown-item" href="#">
                Guru
              </a>
            </li>
          </ul>
        </li>
        <li>
          <a href="#submenu1" className="nav-link px-sm-0 px-2 text-secondary">
            <i className="fs-5 bi-file-earmark-ruled"></i>
            <span className="ms-1 d-none d-sm-inline">Data Tahun Ajar</span>
          </a>
        </li>
        <li>
          <a href="#submenu1" className="nav-link px-sm-0 px-2 text-secondary">
            <i className="fs-5 bi-file-earmark-ruled"></i>
            <span className="ms-1 d-none d-sm-inline">Data Mata Pelajaran</span>
          </a>
        </li>
        <li>
          <a href="#submenu1" className="nav-link px-sm-0 px-2 text-secondary">
            <i className="fs-5 bi-file-earmark-ruled"></i>
            <span className="ms-1 d-none d-sm-inline">Data Kelas</span>
          </a>
        </li>
        <li>
          <a href="#submenu1" className="nav-link px-sm-0 px-2 text-secondary">
            <i className="fs-5 bi-file-earmark-ruled"></i>
            <span className="ms-1 d-none d-sm-inline">Data Jadwal</span>
          </a>
        </li>
        <li>
          <a href="#submenu1" className="nav-link px-sm-0 px-2 text-secondary">
            <i className="fs-5 bi-file-earmark-ruled"></i>
            <span className="ms-1 d-none d-sm-inline">Data Absensi</span>
          </a>
        </li>
        <li>
          <a href="#submenu1" className="nav-link px-sm-0 px-2 text-secondary">
            <i className="fs-5 bi-file-earmark-text"></i>
            <span className="ms-1 d-none d-sm-inline">Rekap Absensi</span>
          </a>
        </li>
      </ul>
      <div className="dropdown py-sm-4 mt-sm-auto ms-auto ms-sm-0 flex-shrink-1">
        <a
          href="#"
          className="d-flex align-items-center text-decoration-none dropdown-toggle text-secondary"
          id="dropdownUser1"
          data-bs-toggle="dropdown"
          aria-expanded="false"
        >
          <img
            src={defaultImage}
            alt="default user"
            width="28"
            height="28"
            className="rounded-circle"
          />
          <span className="d-none d-sm-inline mx-1">Joe</span>
        </a>
        <ul
          className="dropdown-menu dropdown-menu-dark text-small shadow"
          aria-labelledby="dropdownUser1"
        >
          <li>
            <button
              className="dropdown-item"
              onClick={() => navigate("/profile")}
            >
              Profle
            </button>
          </li>
          <li>
            <hr className="dropdown-divider" />
          </li>
          <li>
            <button className="dropdown-item" onClick={handleLogout}>
              Logout
            </button>
          </li>
        </ul>
      </div>
    </div>
  );
}
