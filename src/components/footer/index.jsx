import React from "react";

export default function Footer() {
  return (
    <footer className="row bg-light py-4 mt-auto text-center">
      <div className="col">
        Copyright © {new Date().getFullYear()} Si Akad | All Rights Reserved
      </div>
    </footer>
  );
}
